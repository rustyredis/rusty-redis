extern crate rusty_client;
use rusty_client::{AsyncClient, parser};
use std::net::SocketAddr;
use std::io::BufRead;
use std::error::Error;
use futures::stream::{FuturesUnordered, TryStreamExt, StreamExt};
use std::time::Instant;
use async_std::task;

use rusty_client::{Options, SubOptions};

use structopt::StructOpt;


fn main() {
  let Options{port, host, cmd} = Options::from_args();
  let addr: SocketAddr = (host,port).into();
  match cmd {
    Some(SubOptions::Benchmark {n_clients, n_iterations}) => {
      bench_get(addr, n_clients, n_iterations);
    },
    _ => {
      async_terminal_client(addr).unwrap();
    }
  }
  // bench_get(64);
  // async_terminal_client(([127,0,0,1], 7878).into()).unwrap();

}

pub fn bench_get(socket: SocketAddr, n_clients: u32, n_iterations: u32) {
  task::block_on(async {
    let clients: FuturesUnordered<_> =
      (0..n_clients)
      .map(|_| AsyncClient::new(socket))
      .collect();
    let mut v: Vec<AsyncClient> = clients.try_collect().await.unwrap();
    let finished: FuturesUnordered<_> = v.iter_mut()
      .map(|client| bench_get_internal(client, n_iterations / n_clients))
      .collect();
    let start_time = Instant::now();
    finished.collect::<()>().await;
    let bench_len = Instant::now() - start_time;
    println!("took {:?}, for {} iterations per second",
      bench_len,
      n_iterations as f64 / bench_len.as_secs_f64());
  });
  //unimplemented!()
}

async fn bench_get_internal(client: &mut AsyncClient, iteration_count: u32) {
  for _ in 0..iteration_count {
    client.get("asdf".into()).await.unwrap();
  }
}

pub fn async_terminal_client(addr: SocketAddr) -> Result<(), Box<dyn Error>> {
    let mut client: AsyncClient = task::block_on(AsyncClient::new(addr))?;
    println!("connection established with server");
    //so we don't keep locking and unlocking stdin
    let stdin_handle = std::io::stdin();
    let mut stdin = stdin_handle.lock();
    let mut s = String::new();
    loop {
        match stdin.read_line(&mut s) {
            Ok(0) => break, //EOF
            Ok(_) => {
                if s == "EXIT\n" {
                    return Ok(());
                }
                let r: Result<(), Box<dyn Error>> = (|| {
                    let query = parser::parse_query(&s)?;
                    let resp = task::block_on(client.send_command(&query))?;
                    println!("{:?}", resp);
                    Ok(())
                })();
                match r {
                    Err(e) => eprintln!("Error occurred: {}", e),
                    _ => (),
                }
            }
            Err(e) => return Err(e.into()),
        }
        s.clear();
    }
    Ok(())
}