use std::error::Error;
use std::fmt::{self, Display, Formatter};

use rusty_redis_common::{Command, DbValue};

mod stringsep;
use stringsep::StringTokenizer;

#[derive(PartialEq, Eq, Debug)]
pub enum QueryError {
  ParseFailure,
}

impl Display for QueryError {
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    write!(f, "ParseFailure")
  }
}

impl Error for QueryError {}

/**
 * Given a client query as a String, gives back the associated command
 * or an error if the query is unrecognized.
 */
pub fn parse_query(query: &str) -> Result<Command, QueryError> {
  use self::Command::*;
  let mut iter = StringTokenizer::new(query); //Iterator over all "tokens" in the query
                                              //get the first word. If there is no first word, return Err(ParseFailure). read the docs for std::option::ok_or()
  let first_word = iter.next().ok_or(QueryError::ParseFailure)?;
  //get the second word as a &str and turn it into a heap-allocated string
  let second_word = iter.next().ok_or(QueryError::ParseFailure)?.to_string();
  //get (maybe) the third token of the query. It's possible that it does not exist, iter.next() returns an Option<&str>, so may be none
  let third_word = iter.next();
  let fourth_word = iter.next();
  //Determine what the command is based on the first word and return a new Command struct
  match first_word {
    "INCR" => Ok(INCR(second_word)),
    "DECR" => Ok(DECR(second_word)),
    "GET" => Ok(GET(second_word)),
    "DEL" => Ok(DEL(second_word)),
    "LPOP" => Ok(LPOP(second_word)),
    "RPOP" => Ok(RPOP(second_word)),
    other => {
      let data_type = third_word.ok_or(QueryError::ParseFailure)?;
      let value = fourth_word.ok_or(QueryError::ParseFailure)?;
      let value_to_send = parse_db_value(data_type, value)?;
      match other {
        "SET" => Ok(SET(second_word, value_to_send)),
        "LPUSH" => Ok(LPUSH(second_word, value_to_send)),
        "RPUSH" => Ok(RPUSH(second_word, value_to_send)),
        // _ matches everything that's not "INCR", "DECR", "GET", or "SET". the first word is none of these, then we don't know how to
        // parse it, so return Err
        _ => Err(QueryError::ParseFailure),
      }
    }
  }
  //note that then entirety of the above `match` block is 1 expression. Since it's the last expression in the function, its value is
  //what is returned
}

fn parse_db_value(data_type: &str, data_value: &str) -> Result<DbValue, QueryError> {
  match data_type {
    "INT" => {
      let inner = data_value
        .parse::<i64>()
        .map_err(|_| QueryError::ParseFailure)?;
      Ok(DbValue::Int(inner))
    }
    "FLOAT" => {
      let inner = data_value
        .parse::<f64>()
        .map_err(|_| QueryError::ParseFailure)?;
      Ok(DbValue::Float(inner))
    }
    "STRING" => Ok(DbValue::Str(data_value.to_string().into_boxed_str().into())),
    "BYTESTRING" => {
      let inner = hex::decode(data_value).map_err(|_| QueryError::ParseFailure)?;
      Ok(DbValue::Bytes(inner.into_boxed_slice().into()))
    }
    _ => Err(QueryError::ParseFailure),
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  pub fn test_parser_basic() {
    use self::Command::*;
    assert_eq!(
      parse_query("GET \"hello world\""),
      Ok(GET("hello world".to_string()))
    );
    assert_eq!(
      parse_query("SET \"byte string item\" BYTESTRING 15"),
      Ok(SET(
        "byte string item".to_string(),
        DbValue::Bytes(vec![21].into_boxed_slice())
      ))
    );
  }
}
