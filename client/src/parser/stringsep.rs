/**
 * StringTokenizer takes a &str and iterates through space-separated words or quoted strings
 * For example, a StringTokenizer over'SET "the key" value' would yeild "SET", then "the key", 
 * and finally "value"
 */
pub struct StringTokenizer<'a> {
  buffer: &'a str,
}

impl<'a> StringTokenizer<'a> {
  pub fn new(s: &'a str) -> StringTokenizer<'a> {
    StringTokenizer { buffer: s }
  }
}


impl<'a> Iterator for StringTokenizer<'a> {
  type Item = &'a str;
  fn next(&mut self) -> Option<Self::Item> {
    //remove whitespace at start
    self.buffer = self.buffer.trim_start();
    //if the first character is a quote, we handle it specially, else we just get the next word
    let first_char = self.buffer.chars().next();
    match first_char {
      Some('"') => {
        let (_head, tail) = self.buffer.split_at(1);
        if let Some(next_quote) = tail.find('"') {
          self.buffer = &tail[next_quote + 1..];
          //return a slice from the char after the quote up to the next quote
          Some(&tail[0..next_quote])
        } else {
          //if we don't find a matching quote, the iterator is done and we discard what we found
          None
        }
      },
      //the next char was not a quote, so find the next whitespace and split before that
      Some(_) => {
        if let Some(index) = self.buffer.find(|c:char| c.is_whitespace()) {
          let (head, tail) = self.buffer.split_at(index);
          self.buffer = tail;
          Some(head)
        } else { //no spaces left in the string, so we'll return the buffer
          let tmp = self.buffer;
          self.buffer = &self.buffer[self.buffer.len().. self.buffer.len()];
          Some(tmp)
        }
      },
      //if there was no first character, we're at the end of our buffer, so return None, signifying that
      //this iterator will not yeild any more values
      None => None,
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
  #[test]
  fn test_strsep() {
    let s = String::from(" get the \"big thing\" done           ");
    let v = vec!["get", "the", "big thing", "done"];
    let tv = StringTokenizer::new(&s).collect::<Vec<_>>();
    assert_eq!(v, tv);
  }
}
