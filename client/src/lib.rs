use std::error::Error;
use std::net::SocketAddr;
use std::fmt;
use rusty_redis_common::{Command, Response, DbValue, AsyncBincodeCodec};

use async_std::net::TcpStream;


pub mod parser;


use structopt::StructOpt;
use std::net::IpAddr;

#[derive(StructOpt, Debug)]
#[structopt(about = "clients and benchmarks for rusty redis")]
pub struct Options {
  #[structopt(short, default_value="7878")]
  pub port: u16,
  #[structopt(short, default_value="127.0.0.1")]
  pub host: IpAddr,
  #[structopt(subcommand)]
  pub cmd: Option<SubOptions>,
}

#[derive(StructOpt, Debug)]
pub enum SubOptions {
  Benchmark { 
    #[structopt(short = "c")]
    n_clients: u32,
    #[structopt(short = "i")]
    n_iterations: u32
  }
}


pub struct AsyncClient {
  net_interface: AsyncBincodeCodec<TcpStream>
}

#[derive(Debug)]
pub enum AsyncClientError {
  Io(std::io::Error),
  Ser(bincode::Error),
  Custom(&'static str)
}

impl fmt::Display for AsyncClientError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}

impl From<&'static str> for AsyncClientError {
  fn from(s: &'static str) -> Self {
    AsyncClientError::Custom(s)
  }
}

impl From<bincode::Error> for AsyncClientError {
  fn from(e: bincode::Error) -> Self {
    AsyncClientError::Ser(e)
  }
}

impl From<std::io::Error> for AsyncClientError {
  fn from(e: std::io::Error) -> Self {
    AsyncClientError::Io(e)
  }
}

impl Error for AsyncClientError {}


impl AsyncClient {
  pub async fn new(addr: SocketAddr) -> Result<AsyncClient, AsyncClientError> {
    let stream = TcpStream::connect(addr).await?;
    Ok(AsyncClient {
      net_interface: stream.into()
    })
  }
  pub async fn send_command(&mut self, cmd: &Command) -> Result<Response, AsyncClientError> {
    let mut buf = Vec::new();
    self.net_interface.write_one(&cmd, &mut buf).await?;
    Ok(self.net_interface.read_one(&mut buf).await?)
  }

  ///retrieves the key, erroring if it could not send or receive properly.
  ///Will return a `Response` object from the database, with a `Failure` option if
  ///the key was not present
  pub async fn get(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::GET(key);
    self.send_command(&cmd).await
  }

  ///sets the key, erroring if it could not send or receive properly.
  ///Will return a `Response` object from the database
  pub async fn set(&mut self, key: String, value: DbValue) -> Result<Response, AsyncClientError> {
    let command = Command::SET(key, value);
    self.send_command(&command).await
  }

  ///atomically increments the key, erroring if it could not send or receive properly.
  ///Will return a `Response` object from the database. For now, `Response::Failure`
  ///returns when the key is not in the database or cannot be parsed as a 128 bit integer
  pub async fn incr(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::INCR(key);
    self.send_command(&cmd).await
  }

  ///atomically decrements the key, erroring if it could not send or receive properly.
  ///Will return a `Response` object from the database, with the `Faulure` option if
  ///the key was not present or couldn't be parsed as a signed 128 bit integer
  pub async fn decr(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::DECR(key);
    self.send_command(&cmd).await
  }

  ///removes a key from the database. Will error if a network error occurs
  pub async fn del(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::DEL(key);
    self.send_command(&cmd).await
  }
  ///pushes a string to the front of a list
  pub async fn lpush(&mut self, key: String, value: DbValue) -> Result<Response, AsyncClientError> {
    let cmd = Command::LPUSH(key, value);
    self.send_command(&cmd).await
  }

  ///pushes a string to the end of a list
  pub async fn rpush(&mut self, key: String, value: DbValue) -> Result<Response, AsyncClientError> {
    let cmd = Command::RPUSH(key, value);
    self.send_command(&cmd).await
  }

  ///pops a string at the end of a list, Erroring if the list does not exist
  pub async fn rpop(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::RPOP(key);
    self.send_command(&cmd).await
  }

  ///pops a string at the front of a list, Erroring if the list does not exist
  pub async fn lpop(&mut self, key: String) -> Result<Response, AsyncClientError> {
    let cmd = Command::LPOP(key);
    self.send_command(&cmd).await
  }
}
