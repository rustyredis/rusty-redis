use std::io;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::net::TcpStream;

#[no_mangle]
pub extern fn rusty_client() -> i32 {
  match run_client_internal() {
    Ok(_) => 0,
    Err(_) => -1
  }
}

fn run_client_internal() -> Result<(), Box<std::error::Error>> {
  let mut stream = TcpStream::connect("127.0.0.1:7878")?;
  println!("connection established with server");
  //so we don't keep locking and unlocking stdin
  let stdin_handle = io::stdin();
  let mut stdin = stdin_handle.lock();
  let mut s = String::new();
  loop {
    match stdin.read_line(&mut s) {
      Ok(0) => break, //EOF
      Ok(_) => {
        if s == "EXIT\n" {
          return Ok(());
        }
        let mut read_buffer = Vec::<u8>::new();
        stream.write_all(s.as_ref())?;
        let mut b = BufReader::new(stream);
        b.read_until(b'\n', &mut read_buffer)?;
        stream = b.into_inner();
        print!("{}", std::str::from_utf8(&read_buffer)?);
      }
      Err(_) => break
    }
    s.clear();
  }
  Ok(())

}