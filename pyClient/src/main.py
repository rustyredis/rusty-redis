from ctypes import cdll
from sys import platform

if platform == 'darwin':
    prefix = 'lib'
    ext = 'dylib'
elif platform == 'win32':
    prefix = ''
    ext = 'dll'
else:
    prefix = 'lib'
    ext = 'so'

lib = cdll.LoadLibrary('target/debug/{}rusty_client.{}'.format(prefix, ext))
rusty_client = lib.rusty_client


output = rusty_client()
print("output is:")
print(rusty_client)