use serde::{Deserialize, Serialize, de::DeserializeOwned};
use std::collections::linked_list::LinkedList;
use std::collections::HashMap;
use std::sync::Arc;
use std::io::{self, ErrorKind};
use futures::io::{AsyncReadExt, AsyncWriteExt};
use byteorder::{NetworkEndian, ByteOrder};
use std::borrow::BorrowMut;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum Command {
  INCR(String),
  DECR(String),
  SET(String, DbValue),
  GET(String),
  DEL(String),
  LPUSH(String, DbValue),
  RPUSH(String, DbValue),
  LPOP(String),
  RPOP(String),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum QueryErr {
  IllegalOperation,
  NotPresent,
  KeyNotEmpty,
  NotImplemented,
  EmptyCollection
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum DbValue {
  Str(Arc<str>),
  Int(i64),
  Float(f64),
  Bytes(Arc<[u8]>)
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum DbStructure {
  List(LinkedList<DbValue>),
  Map(HashMap<Box<str>, DbValue>)
}



#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Response {
  Confirmation,
  Singular(DbValue),
  Array(Vec<DbValue>),
  Failure(QueryErr)
}

impl Response {
  pub fn is_success(&self) -> bool {
    match self {
      Response::Failure(_) => false,
      _ => true
    }
  }
  pub fn is_failure(&self) -> bool {
    !self.is_success()
  }
}

pub struct AsyncBincodeCodec<T> {
  inner: T
}

impl<T> AsyncBincodeCodec<T>
where T: AsyncReadExt + AsyncWriteExt + Unpin
{
  pub async fn read_one<D>(&mut self, v: &mut Vec<u8>) -> io::Result<D>
  where D: DeserializeOwned
  {
    let mut len_buf = [0u8;4];
    self.inner.read_exact(&mut len_buf).await?;
    let message_len = NetworkEndian::read_u32(&len_buf) as usize;
    v.resize(message_len, 0);
    self.inner.read_exact(v).await?;
    bincode::deserialize(v.as_ref()).map_err(|_| ErrorKind::Other.into())
  }

  pub async fn write_one<D>(&mut self, item: &D, v: &mut Vec<u8>) -> io::Result<()>
  where D: Serialize
  {
    v.clear();
    v.extend_from_slice(&[0,0,0,0]);
    let v_ref: &mut Vec<u8> = v.borrow_mut();
    bincode::serialize_into(v_ref, item).map_err(|_| ErrorKind::Other)?;
    let vlen = (v.len() - 4) as u32;
    NetworkEndian::write_u32(&mut v[0..4], vlen);
    self.inner.write_all(&v).await
  }
}

impl <T> From<T> for AsyncBincodeCodec<T> {
  fn from(inner: T) -> Self {
    AsyncBincodeCodec { inner }
  }
}

