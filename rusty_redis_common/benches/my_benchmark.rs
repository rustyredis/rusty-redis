#[macro_use]
extern crate criterion;
extern crate rusty_redis_common;

use criterion::Criterion;

use rusty_redis_common::{Command, DbValue};

macro_rules! cbor_bench {
  ($x: expr, $y: ident) => {
    fn $y(c: &mut Criterion) {
      let test_command = $x;
      let serialized_cmd = serde_cbor::to_vec(&test_command).unwrap();
      c.bench_function(stringify!($y), |b| {
        b.iter(|| {
          let _: Command = serde_cbor::from_slice(&serialized_cmd).unwrap();
        })
      });
    }
  };
}

macro_rules! bincode_bench {
  ($x: expr, $y: ident) => {
    fn $y(c: &mut Criterion) {
      let test_command = $x;
      let mut v = vec![];
      bincode::serialize_into(&mut v, &test_command).unwrap();
      c.bench_function(stringify!($y), |b| {
        b.iter(|| {
          let _: Command = bincode::deserialize(&v).unwrap();
        })
      });
    }
  };
}

bincode_bench!(
  Command::LPUSH("asdf".into(), DbValue::Str("whatever".into())),
  bincode_str
);

cbor_bench!(
  Command::LPUSH("asdf".into(), DbValue::Str("whatever".into())),
  cbor_str
);

bincode_bench! {
  Command::SET("asdf".into(), DbValue::Int(5)),
  bincode_int
}

cbor_bench! {
  Command::SET("asdf".into(), DbValue::Int(5)),
  cbor_int
}

bincode_bench! {
  Command::INCR("asdf".into()),
  bincode_incr
}

cbor_bench! {
  Command::INCR("asdf".into()),
  cbor_incr
}

criterion_group!(
  benches,
  bincode_str,
  cbor_str,
  bincode_int,
  cbor_int,
  bincode_incr,
  cbor_incr
);
criterion_main!(benches);
