#![type_length_limit="5000000"]
extern crate rusty_server;
use rusty_server::db::DB;
use async_std::task;

#[macro_use]
extern crate clap;

fn main() {
  //TODO figure out how to use this command line argument parser thing
  let matches = clap_app!(rustyserver =>
    (version: "1.0")
    (author:"Alex Schuler. <ajs520@lehigh.edu>, Corey Loevinger. <cpl220@lehigh.edu>, Sam Ruhe <sar420@lehigh.edu>, Ab Stity <afs220@lehigh.edu>")
    (about: "Redis, but parallel, and no scripting or clustering or anything")
    (@arg port: -p --port +takes_value)
  ).get_matches();

  let port: u16 = matches.value_of("port").and_then(|v| v.parse::<u16>().ok()).unwrap_or(7878);
  let db = Box::new(DB::new());
  //leaking the database, since it will have to live for the whole life of the program
  let db: &'static DB = Box::leak(db);
  task::block_on(db.run_async(port)).unwrap();
}

