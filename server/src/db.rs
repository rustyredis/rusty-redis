use lockfree::map::{Insertion, Map, Preview};

use serde::ser::{SerializeMap, Serializer};
use serde::{Deserialize, Serialize};
use chrono::Local;

use futures::stream::StreamExt;


use rusty_redis_common::{
  Command,
  DbValue::{self, *},
  QueryErr::*,
  Response,
  AsyncBincodeCodec
};

use im::vector;

use std::error::Error;
use std::net::SocketAddr;
use std::fs::OpenOptions;
use std::time::Duration;
use std::io;

use nix::unistd::{fork, ForkResult};

use self::DbEntry::*;
use self::DbStructure::*;

use async_std::task;
use async_std::net::{TcpListener, TcpStream};
use async_std::future::join;

//Good thing we have `const fn' now
const WRITEBACK_INTERVAL: Duration = Duration::from_secs(300);

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum DbEntry {
  Primitive(DbValue),
  Collection(DbStructure),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum DbStructure {
  List(im::Vector<DbValue>),
  Map(im::HashMap<Box<str>, DbValue>),
}

pub struct DB {
  map: Map<String, DbEntry>,
}

impl DB {
  pub fn new() -> Self {
    DB { map: Map::new() }
  }

  #[inline(never)] //for performance analysis
  pub fn handle_query(&self, query: Command) -> Response {
    use Command::*;
    match query {
      GET(k) => self.handle_get(k),
      SET(k, v) => self.handle_set(k, v),
      INCR(k) => self.apply_int_arithmetic(k, |x| x + 1),
      DECR(k) => self.apply_int_arithmetic(k, |x| x - 1),
      DEL(k) => self.handle_delete(k),
      LPUSH(l, v) => self.handle_lpush(l, v),
      RPUSH(l, v) => self.handle_rpush(l, v),
      LPOP(l) => self.handle_lpop(l),
      RPOP(l) => self.handle_rpop(l),
    }
  }

  #[allow(unused)]
  fn handle_list_removal<F>(&self, list_name: String, op: F) -> Response
  where
    F: Fn(&im::Vector<DbValue>) -> (Response, Preview<DbEntry>),
  {
    let mut return_value = Response::Failure(NotPresent);
    self
      .map
      .insert_with(list_name, |_old_key, _opt_old_value, opt_old_pair| {
        match opt_old_pair {
          Some((_key, Collection(List(lst)))) => {
            let (rv, preview) = op(lst);
            return_value = rv;
            preview
          }
          Some((_key, _)) => {
            //exists but of wrong type
            return_value = Response::Failure(IllegalOperation);
            Preview::Keep
          }
          None => Preview::Keep,
        }
      });
    return_value
  }

  fn handle_lpop(&self, list_name: String) -> Response {
    self.handle_list_removal(list_name, |lst: &im::Vector<DbValue>| {
      let mut new_list = lst.clone();
      if let Some(v) = new_list.pop_front() {
        (
          Response::Singular(v),
          Preview::New(Collection(List(new_list))),
        )
      } else {
        (Response::Failure(EmptyCollection), Preview::Keep)
      }
    })
  }

  fn handle_rpop(&self, list_name: String) -> Response {
    self.handle_list_removal(list_name, |lst: &im::Vector<DbValue>| {
      let mut new_list = lst.clone();
      if let Some(v) = new_list.pop_back() {
        (
          Response::Singular(v),
          Preview::New(Collection(List(new_list))),
        )
      } else {
        (Response::Failure(EmptyCollection), Preview::Keep)
      }
    })
  }

  fn handle_lpush(&self, list_name: String, v: DbValue) -> Response {
    let mut return_value = Response::Failure(NotPresent);
    self
      .map
      .insert_with(list_name, |_old_key, _opt_old_value, opt_old_pair| {
        let to_insert = v.clone(); //cheap because Arc over str and Box<[u8]> types
        match opt_old_pair {
          //nothing in there, so create a new vector and insert that
          None => {
            return_value = Response::Confirmation;
            Preview::New(Collection(List(vector![to_insert])))
          }
          Some((_key, Collection(List(old_vec)))) => {
            //there was a list in there, so prepend
            let mut nv = old_vec.clone();
            nv.push_front(to_insert);
            return_value = Response::Confirmation;
            Preview::New(Collection(List(nv)))
          }
          _ => {
            //there was something in there that wasn't a list, so keep it and error
            return_value = Response::Failure(IllegalOperation);
            Preview::Keep
          }
        }
      });
    return_value
  }

  fn handle_rpush(&self, list_name: String, v: DbValue) -> Response {
    let mut return_value = Response::Failure(NotPresent);
    self
      .map
      .insert_with(list_name, |_old_key, _opt_old_value, opt_old_pair| {
        let to_insert = v.clone(); //cheap because Arc over str and Box<[u8]> types
        match opt_old_pair {
          //nothing in there, so create a new vector and insert that
          None => {
            return_value = Response::Confirmation;
            Preview::New(Collection(List(vector![to_insert])))
          }
          Some((_key, Collection(List(old_vec)))) => {
            //there was a list in there, so prepend
            let mut nv = old_vec.clone();
            nv.push_back(to_insert);
            return_value = Response::Confirmation;
            Preview::New(Collection(List(nv)))
          }
          _ => {
            //there was something in there that wasn't a list, so keep it and error
            return_value = Response::Failure(IllegalOperation);
            Preview::Keep
          }
        }
      });
    return_value
  }

  fn handle_get(&self, k: String) -> Response {
    match self.map.get(&k) {
      Some(pair) => match pair.val() {
        Primitive(x) => Response::Singular(x.clone()),
        _ => Response::Failure(IllegalOperation),
      },
      None => Response::Failure(NotPresent),
    }
  }
  fn handle_set(&self, k: String, v: DbValue) -> Response {
    //insert. If we get something back, then we know we replaced something. see documentation for Option::map_or_else
    //HashMap::insert returns an Option<V> signaling if something was replaced or not
    let txn_res = self.map.insert(k, Primitive(v));
    match txn_res {
      Some(v) => match v.val() {
        Primitive(prim) => Response::Singular(prim.clone()),
        _ => Response::Confirmation,
      },
      _ => Response::Confirmation,
    }
  }

  fn apply_int_arithmetic<F>(&self, k: String, f: F) -> Response
  where
    F: Fn(i64) -> i64,
  {
    let mut err_flag = NotPresent;
    let tx_res = self.map.insert_with(
      k,
      |_old_key, _opt_old_value, opt_old_pair| match opt_old_pair {
        Some((_key, Primitive(Int(x)))) => Preview::New(Primitive(Int(f(*x)))),
        Some((_key, _)) => {
          err_flag = IllegalOperation;
          Preview::Keep
        }
        None => {
          err_flag = NotPresent;
          Preview::Keep
        }
      },
    );
    match tx_res {
      Insertion::Updated(removed_value) => match removed_value.val() {
        Primitive(val @ Int(_)) => Response::Singular(val.clone()),
        _ => Response::Failure(IllegalOperation),
      },
      _ => Response::Failure(err_flag),
    }
  }

  fn handle_delete(&self, k: String) -> Response {
    let tx_result = self.map.remove_with(&k, |(_k, v)| match v {
      Primitive(_) => true,
      _ => false,
    });
    match tx_result {
      Some(removed) => match removed.val() {
        Primitive(v) => Response::Singular(v.clone()),
        _ => unreachable!(),
      },
      _ => Response::Failure(IllegalOperation), //can't delete a collection, must use drop
    }
  }

  pub async fn run_async(&'static self, port: u16) -> Result<(), Box<dyn Error>> {
    let addr: SocketAddr = ([127,0,0,1], port).into();
    let listener = TcpListener::bind(addr).await?;
    let listen_fut = listener.incoming().for_each_concurrent(0, |connection_res| async {
      match connection_res {
        Err(e) => eprintln!("Error accepting connection: {}", e),
        Ok(socket) => {
          let close_res = task::spawn(async move {
            self.handle_connection(socket).await
          }).await;
          if let Err(e) = close_res {
            eprintln!("Connection with client closed: {}", e);
          }
        }
      }
    });
    let persist_fut = async {
      loop {
        task::sleep(WRITEBACK_INTERVAL).await;
        self.fork_save();
      }
    };
    join!(listen_fut, persist_fut).await;
    Ok(())
  }

  async fn handle_connection(&'static self, socket: TcpStream) -> Result<(), io::Error> {
    let mut buf = Vec::new();
    let mut framed = AsyncBincodeCodec::from(socket);
    loop {
      let cmd = framed.read_one(&mut buf).await?;
      let response = self.handle_query(cmd);
      framed.write_one(&response, &mut buf).await?;
    }
  }

  fn fork_save(&'static self) {
    match fork() {
      Ok(ForkResult::Parent { .. }) => (),
      Ok(ForkResult::Child) => {
        let self_ptr = self as *const DB;
        std::mem::forget(self_ptr);
        //safe because this is a forked process and we know that self_ptr is the
        //only reference held to the map
        let owned_self_copy = unsafe { std::ptr::read(self_ptr) };
        owned_self_copy.write_back_to_disk();
        std::process::exit(0);
      }
      Err(_) => panic!("how the fuck does fork fail"),
    }
  }

  fn write_back_to_disk(self) {
    println!("child process about to serialize");
    let current_time = format!(
      "{}{}",
      (Local::now()).format("%Y-%m-%d_%H-%M-%S"),
      ".json"
    );
    let res: Result<(), Box<dyn Error>> = (|| {
      let map_iter = self.map.into_iter();
      let out_file = OpenOptions::new()
        .create(true)
        .write(true)
        .open(current_time)?;
      let mut ser = serde_json::Serializer::new(out_file);
      let mut seq = ser.serialize_map(None)?;
      for (key, value) in map_iter {
        seq.serialize_entry(&key, &value)?;
      }
      // seq.end()?;
      SerializeMap::end(seq)?;
      Ok(())
    })();
    match res {
      Ok(_) => println!("successfully wrote out the file"),
      Err(e) => println!("failed to serialize: {}", e),
    }
  }
}
